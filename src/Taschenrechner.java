import java.util.Scanner;

public class Taschenrechner {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("x: ");
        int x = sc.nextInt();

        System.out.print("y: ");
        int y = sc.nextInt();

        System.out.print("op: ");
        char op = sc.next().charAt(0);

        switch (op) {

        case '+' -> System.out.println(x + y);
        case '-' -> System.out.println(x - y);
        case '/' -> System.out.println(x / y);
        case '*' -> System.out.println(x * y);
        default -> System.out.println("Keine gute Zahl");
        }

    }

}
