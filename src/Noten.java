import java.util.Scanner;

public class Noten {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        char r = sc.nextLine().charAt(0);

        switch (r) {
        case 'I' -> System.out.println(1);
        case 'V' -> System.out.println(5);
        case 'X' -> System.out.println(10);
        case 'L' -> System.out.println(50);
        case 'C' -> System.out.println(100);
        case 'D' -> System.out.println(500);
        case 'M' -> System.out.println(1000);
        default -> System.out.println("Keine gute zahl");
        }

    }

}