import java.util.Scanner;

public class OhmschesGesetz {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Was willst du berechnen? (R|U|I)");

        char a = sc.next().charAt(0);

        switch (a) {
        case 'R' -> System.out.println(requestU(sc) / requestI(sc));
        case 'U' -> System.out.println(requestR(sc) * requestI(sc));
        case 'I' -> System.out.println(requestU(sc) / requestR(sc));
        default -> System.out.println("Keine gute Zahl");
        }

    }

    static int requestU(Scanner sc) {
        return sc.nextInt();
    }

    static int requestI(Scanner sc) {
        return sc.nextInt();
    }

    static int requestR(Scanner sc) {
        return sc.nextInt();
    }

}
