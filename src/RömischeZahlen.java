import java.util.Scanner;

public class RömischeZahlen {

    static String order = "IVXLCDM";

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.print("Römische Formel: ");
        String input = sc.next();

        char[] split = input.toCharArray();

        int ergebnis = 0;

        // count to check for repeated numbers
        int count = 0;

        for (int i = 1; i < split.length; i++) {
            int x = romToInt(split[i]);
            int last = romToInt(split[i - 1]);

            // max 3 Zeichen rule
            if (x == last)
                count++;

            if (count > 2) {
                System.out.println("Maximum von 3 überschritten");
                System.exit(1);
            }

            if (x > last) {
                ergebnis -= last;
            } else {
                ergebnis += last;
            }

            if (i == split.length - 1) {
                ergebnis += x;
            }
        }
        System.out.println(ergebnis);
    }

    static int romToInt(char rom) {
        return switch (rom) {
        case 'I' -> 1;
        case 'V' -> 5;
        case 'X' -> 10;
        case 'L' -> 50;
        case 'C' -> 100;
        case 'D' -> 500;
        case 'M' -> 1000;
        default -> 0;
        };
    }

}
